json.extract! restaurant, :id, :name, :rating, :user_id, :created_at, :updated_at
json.url restaurant_url(restaurant, format: :json)
